#!/usr/bin/bash

input_vcf=$1
vcf_dir="$(dirname $input_vcf)"
#cd vcf_dir
#mkdir sort_dir
#mv vcf_dir/*tsv sort_dir
#mv vcf_dir/*pdf sort_dir
#mv vcf_dir/*/ sort_dir

# python3  
snakemake --cores 8 --config config.yaml --config input_file="$input_vcf"
