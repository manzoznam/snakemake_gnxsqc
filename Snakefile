# Snakefile

import os
import glob

def list_cov_files(directory):
    return glob.glob(os.path.join(directory, '**', 'cov.txt'), recursive=True)





# Rule all defines the final output
rule all:
    input:
        "/mnt/NGS_Diagnostik/DEVELOPMENT/snakemake/Genexus/combined_coverage_report.txt"

# Rule to perform coverage analysis on all samples and generate a single report
rule combine_coverage:
    input: cov="/mnt/NGS_Diagnostik/DEVELOPMENT/snakemake/Genexus/covs.yaml", pipeon="/home/watchdog/github_app/snakemake_workflows/gnxsqc/scripts/python_extract_run_metrics.py"
    output: report="/mnt/NGS_Diagnostik/DEVELOPMENT/snakemake/Genexus/combined_coverage_report.txt"
    shell:
        "python3 {input.pipeon} {input.cov} {output.report}"

