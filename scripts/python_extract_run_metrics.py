# Define the metrics to extract
import os, re, sys
import pandas as pd
import yaml
#pattern = re.compile(r"\.stats\.cov\.txt$")
config_file=sys.argv[1]
output_file=sys.argv[2]


with open(config_file, 'r') as file:
    config = yaml.safe_load(file)

filepaths =  [p for p in config['coverage'].values()]

metrics = ["Sample Name", "Average base coverage depth", "Target base coverage at 500x", "Uniformity of base coverage", "Number of mapped reads"]


def read_cov_stats(filepath):
    with open(filepath, 'r') as file:
        lines = file.readlines()
        values_dict = {}
        # Loop through the lines and look for the metrics
        for line in lines:
            # Split the line by whitespace
            words = line.split(':')
            # Check if the first word is a metric
            if words[0] in metrics:
                # Store the value in the dictionary
                category = words[0]
                category = "_".join(category.split(" "))
                item_value =  words[1].replace("\n", "").replace("\t", "")
                item_value = "".join(item_value.split())
                values_dict[category] = item_value
        return values_dict


## Aggregate metrics
rm = [read_cov_stats(fp) for fp in filepaths]

df = pd.DataFrame(rm)
df.to_csv(output_file, sep='\t', index=False, header=True)
# output_file = os.path.join(directory, "Genexus_run_coverage_metrics.tsv")
# df.to_csv(output_file, sep='\t', index=False, header=True)
